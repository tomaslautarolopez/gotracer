package main

import (
	"bytes"
	"fmt"
	"gotracer/entities"
	"os"
	"strconv"
)

func rayColor(r entities.Ray) entities.Color {
	unitDirection := entities.UnitVector(r.Direction)
	t := 0.5 * (unitDirection.Cord.Y + 1.0)

	baseColor1 := entities.Color{R: 1.0, G: 1.0, B: 1.0}
	baseColor2 := entities.Color{R: 0.5, G: 0.7, B: 1.0}

	baseColor1.Mul(1.0 - t)
	baseColor2.Mul(t)

	baseColor1.Sum(&baseColor2)

	return baseColor1
}

func main() {
	aspectRatio := 16.0 / 9.0
	imageWidth := 400
	imageHeight := int(float64(imageWidth) / aspectRatio)

	viewportHeight := 2.0
	viewportWidth := aspectRatio * viewportHeight
	focalLength := 1.0

	origin := entities.Point{Cord: entities.Cordinates{X: 0, Y: 0, Z: 0}}
	horizontal := entities.Vector{Cord: entities.Cordinates{X: float32(viewportWidth), Y: 0, Z: 0}}

	vertical := entities.Vector{Cord: entities.Cordinates{X: 0, Y: float32(viewportHeight), Z: 0}}
	lowerLeftCorner := entities.SustAndGenCordinates(
		&origin.Cord,
		entities.SustAndGenCordinates(
			entities.DivByFactorAndGenCordinates(horizontal.Cord, 2),
			entities.SustAndGenCordinates(
				entities.DivByFactorAndGenCordinates(vertical.Cord, 2),
				&entities.Cordinates{X: 0, Y: 0, Z: float32(focalLength)},
			),
		))

	var image bytes.Buffer

	image.WriteString("P3\n")
	image.WriteString(strconv.Itoa(int(imageWidth)))
	image.WriteString(" ")
	image.WriteString(strconv.Itoa(imageHeight))
	image.WriteString("\n255\n")

	for j := imageHeight - 1; j >= 0; j-- {
		fmt.Println("\rScanlines remaining: ", j)

		for i := 0; i < imageWidth; i++ {
			u := float32(i) / float32(imageWidth-1)
			v := float32(j) / float32(imageHeight-1)
			cordinates := entities.SustAndGenCordinates(
				entities.SumAndGenCordinates(
					&origin.Cord,
					entities.SumAndGenCordinates(lowerLeftCorner, entities.SumAndGenCordinates(
						entities.MulByFactorAndGenCordinates(horizontal.Cord, u),
						entities.MulByFactorAndGenCordinates(vertical.Cord, v),
					)),
				),
				&origin.Cord,
			)
			directionVector := entities.Vector{Cord: *cordinates}

			r := entities.Ray{
				Origin:    origin,
				Direction: directionVector,
			}

			pixelColor := rayColor(r)
			image.WriteString(pixelColor.WriteColor())
		}
	}

	err := os.WriteFile("./image.ppm", image.Bytes(), 0644)

	if err != nil {
		fmt.Println("Error ", err)
	} else {
		fmt.Println("Done")
	}

}
