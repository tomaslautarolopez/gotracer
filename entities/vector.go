package entities

import "math"

type Vector struct {
	Cord Cordinates
}

func (v *Vector) LengthSquared() float32 {
	return v.Cord.X*v.Cord.X + v.Cord.Y*v.Cord.Y + v.Cord.Z*v.Cord.Z
}

func (v *Vector) Op() float32 {
	v.Cord.X = -v.Cord.X
	v.Cord.Y = -v.Cord.Y
	v.Cord.Z = -v.Cord.Z

	return float32(math.Sqrt(float64(v.LengthSquared())))
}

func (v *Vector) Length() float32 {
	return float32(math.Sqrt(float64(v.LengthSquared())))
}

func UnitVector(v Vector) *Vector {

	return &Vector{
		Cord: *DivByFactorAndGenCordinates(v.Cord, v.Length()),
	}
}
