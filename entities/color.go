package entities

import (
	"bytes"
	"math"
	"strconv"
)

type Color struct {
	R float32
	G float32
	B float32
}

func (v *Color) LengthSquared() float32 {
	return v.R*v.R + v.G*v.G + v.B*v.B
}

func (v *Color) Length() float32 {
	return float32(math.Sqrt(float64(v.LengthSquared())))
}

func (v *Color) Op() float32 {
	v.R = -v.R
	v.G = -v.G
	v.B = -v.B

	return float32(math.Sqrt(float64(v.LengthSquared())))
}

func (v *Color) Sum(sv *Color) {
	v.R += sv.R
	v.G += sv.G
	v.B += sv.B
}

func (v *Color) Mul(expo float32) {
	v.R *= expo
	v.G *= expo
	v.B *= expo
}

func (v *Color) Div(expo float32) {
	v.Mul(1 / expo)
}

func (v *Color) WriteColor() string {
	// Write the translated [0,255] value of each color component.
	var colorOutput bytes.Buffer

	colorOutput.WriteString(strconv.Itoa(int(255.999 * v.R)))
	colorOutput.WriteString(" ")
	colorOutput.WriteString(strconv.Itoa(int(255.999 * v.G)))
	colorOutput.WriteString(" ")
	colorOutput.WriteString(strconv.Itoa(int(255.999 * v.B)))
	colorOutput.WriteString("\n")

	return colorOutput.String()
}
