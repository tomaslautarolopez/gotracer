package entities

type Cordinates struct {
	X float32
	Y float32
	Z float32
}

func (v *Cordinates) SumCordinates(sv *Cordinates) {
	v.X += sv.X
	v.Y += sv.Y
	v.Z += sv.Z
}

func (v *Cordinates) MulCordinates(expo float32) {
	v.X *= expo
	v.Y *= expo
	v.Z *= expo
}

func (v *Cordinates) DivCordinates(expo float32) {
	v.MulCordinates(1 / expo)
}

func SumAndGenCordinates(v, u *Cordinates) *Cordinates {
	return &Cordinates{X: v.X + u.X, Y: v.Y + u.Y, Z: v.Z + u.Z}
}

func SustAndGenCordinates(v, u *Cordinates) *Cordinates {
	return &Cordinates{X: v.X - u.X, Y: v.Y - u.Y, Z: v.Z - u.Z}
}

func MulAndGenCordinates(v, u *Cordinates) *Cordinates {
	return &Cordinates{X: v.X * u.X, Y: v.Y * u.Y, Z: v.Z * u.Z}
}

func MulByFactorAndGenCordinates(v Cordinates, factor float32) *Cordinates {
	return &Cordinates{X: v.X * factor, Y: v.Y * factor, Z: v.Z * factor}
}

func DivByFactorAndGenCordinates(v Cordinates, factor float32) *Cordinates {
	return MulByFactorAndGenCordinates(v, 1/factor)
}

func CrossCordinates(u, v Cordinates) *Cordinates {
	return &Cordinates{
		u.Y*v.Z - u.Z*v.Y,
		u.Z*v.X - u.X*v.Z,
		u.X*v.Y - u.Y*v.X,
	}
}

func DotCordinates(v, u Cordinates) float32 {
	return v.X*u.X + v.Y*u.Y + v.Z*u.Z
}
