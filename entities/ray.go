package entities

type Ray struct {
	Origin    Point
	Direction Vector
}

func (r *Ray) At(t float32) Point {
	var p Point

	// Better to use a function that generates a Point?
	p.Cord = *SumAndGenCordinates(&r.Origin.Cord, MulByFactorAndGenCordinates(r.Direction.Cord, t))

	return p
}
